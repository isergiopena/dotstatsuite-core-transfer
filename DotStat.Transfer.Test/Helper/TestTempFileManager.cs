﻿using System;
using System.IO;

namespace DotStat.Transfer.Test.Helper
{
    public sealed class TestTempFileManager : ITempFileManagerBase
    {
        public string GetTempFileName(string filename = null) => Path.Combine(Path.GetTempPath(), filename ?? $"{DateTime.Now:yyyy_MM_dd_HH_mm_ss_ffffff}.tmp");
    }
}
