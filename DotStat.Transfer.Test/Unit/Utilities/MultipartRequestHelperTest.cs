﻿
using System.IO;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Test;
using DotStatServices.Transfer.Utilities;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Net.Http.Headers;
using NUnit.Framework;

namespace DotStat.Transfer.Test.Unit.Utilities
{
    [TestFixture]
    public class MultipartRequestHelperTest : UnitTestBase
    {
        private static readonly FormOptions DefaultFormOptions = new FormOptions();

        public MultipartRequestHelperTest()
        {
            Configuration.DefaultLanguageCode = "en";
        }

        [TestCase("multipart/form-data; boundary =--7t435", "--7t435", true)]
        [TestCase("multipart/form-data; boundary=--324532flkjasdf4e43s", "324532flkjasdf4e43s", false)]
        public void GetBoundaryValueTest(string contentType, string expectedBoundary, bool expectedEqual)
        {
            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(contentType),
                DefaultFormOptions.MultipartBoundaryLengthLimit,
                Configuration.DefaultLanguageCode);

            Assert.IsNotNull(boundary);
            Assert.AreEqual(expectedEqual, boundary==expectedBoundary);
        }

        [TestCase("multipart/form-data; boundary =--7t435", 20, false)]
        [TestCase("multipart/form-data; boundary=--324532flkjasdf4e43s", 5, true)]
        public void GetBoundaryLengthTest(string contentType, int boundaryLimit, bool expectedError)
        {
            if (expectedError)
            {
                var ex = Assert.Throws<DotStatException>(() => MultipartRequestHelper.GetBoundary(
                    MediaTypeHeaderValue.Parse(contentType),
                    boundaryLimit,
                    Configuration.DefaultLanguageCode));

                var expectedMsg = string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.BoundaryLengthLimit,
                        Configuration.DefaultLanguageCode), boundaryLimit);

                Assert.AreEqual(expectedMsg, ex.Message);
            }
            else
                Assert.DoesNotThrow(() => MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(contentType),
                boundaryLimit,
                Configuration.DefaultLanguageCode));
        }

        [TestCase("multipart/form-data; boundary=--7t435",  false)]
        [TestCase("multipart/form-data; boundary=",  true)]
        [TestCase("multipart/form-data;", true)]
        public void GetBoundaryValueTest(string contentType, bool expectedError)
        {
            if (expectedError)
            {
                var ex = Assert.Throws<DotStatException>(() => MultipartRequestHelper.GetBoundary(
                    MediaTypeHeaderValue.Parse(contentType),
                    DefaultFormOptions.MultipartBoundaryLengthLimit,
                    Configuration.DefaultLanguageCode));

                var expectedMsg = LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingContentTypeBoundary, Configuration.DefaultLanguageCode);

                Assert.AreEqual(expectedMsg, ex.Message);
            }
            else
                Assert.DoesNotThrow(() => MultipartRequestHelper.GetBoundary(
                    MediaTypeHeaderValue.Parse(contentType),
                    DefaultFormOptions.MultipartBoundaryLengthLimit,
                    Configuration.DefaultLanguageCode));
        }

        [TestCase("multipart/form-data; ", true)]
        [TestCase(null, false)]
        [TestCase("", false)]
        [TestCase("application/xml", false)] 
        [TestCase("multipart/mixed; ", true)]
        public void IsMultipartContentTypeTest(string contentType, bool testSucceed)
        {
            Assert.AreEqual(testSucceed, MultipartRequestHelper.IsMultipartContentType(contentType));
        }
        
        [TestCase("form-data; name=\"dataspace\"",  false)]
        [TestCase("form-data; name=\"eddFile\"; filename=\"eddFile.xml\"", true)]
        public void ContentDispositionTest(string input, bool isFileType)
        {
            ContentDispositionHeaderValue.TryParse(input, out var contentDisposition);
            Assert.AreEqual(isFileType, MultipartRequestHelper.HasFileContentDisposition(contentDisposition));
            Assert.AreEqual(!isFileType, MultipartRequestHelper.HasFormDataContentDisposition(contentDisposition));
        }
    }
}
