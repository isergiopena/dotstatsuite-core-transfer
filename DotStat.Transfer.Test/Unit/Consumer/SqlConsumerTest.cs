﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Db.Validation;
using DotStat.Db.Validation.SqlServer;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model;
using System.Threading;
using System.Threading.Tasks;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using DotStat.Db;
using System.Transactions;
using DotStat.Common.Exceptions;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Transaction = DotStat.Domain.Transaction;
using DotStat.Common.Localization;

namespace DotStat.Transfer.Test.Unit.Consumer
{
    [TestFixture]
    [TestFixture(TransferType.MetadataOnly ,ReferencedStructureType.DataFlow)]
    [TestFixture(TransferType.DataOnly, ReferencedStructureType.DataFlow)]
    [TestFixture(TransferType.DataAndMetadata, ReferencedStructureType.DataFlow)]
    [TestFixture(TransferType.MetadataOnly, ReferencedStructureType.Dsd)]
    [TestFixture(TransferType.DataOnly, ReferencedStructureType.Dsd)]
    [TestFixture(TransferType.DataAndMetadata, ReferencedStructureType.Dsd)]
    public class SqlConsumerTest : SdmxUnitTestBase
    {
        private readonly Mock<IMappingStoreDataAccess> _mappingStore = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IMetadataStoreRepository> _metadataStoreRepository = new Mock<IMetadataStoreRepository>();
        private readonly Mock<IAuthorizationManagement> _authorisation = new Mock<IAuthorizationManagement>();
        private readonly Mock<ISqlDatasetAttributeDatabaseValidator> _sqlDatasetAttributeDatabaseValidator = new Mock<ISqlDatasetAttributeDatabaseValidator>();
        private readonly Mock<IDatasetAttributeValidator> _datasetAttributeValidator = new Mock<IDatasetAttributeValidator>();
        private readonly Mock<ISqlKeyableDatabaseValidator> _sqlDatabaseValidator = new Mock<ISqlKeyableDatabaseValidator>();
        private readonly Mock<ISqlTransferParam> _param = new Mock<ISqlTransferParam>();

        private readonly Mock<SqlDotStatDb> _dotStatDbMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IDotStatDbService> _dotStatDbServiceMock = new Mock<IDotStatDbService>();

        private readonly IImportReferenceableStructure _referencedStructure;
        private readonly TransferContent _content;
        private readonly SqlConsumer _sqlConsumer;
        private readonly ReferencedStructureType _referencedStructureType;
        private readonly TransferType _transferType;

        public SqlConsumerTest(TransferType transferType, ReferencedStructureType referencedStructureType)
        {
            _referencedStructureType = referencedStructureType;
            _transferType = transferType;
            var dataSpaceId = "DummySpace";
            this.Configuration.MaxTransferErrorAmount = 0;
          
            List<IValidationError> emptyErrorList = new List<IValidationError>();
            List<IValidationError> emptyMergeErrorList = new List<IValidationError>();

            _param = new Mock<ISqlTransferParam>();
            _param.Setup(exp => exp.ValidationType).Returns(ValidationType.ImportWithFullValidation);
            _param.Setup(exp => exp.TransferType).Returns(_transferType);

            //string connectionString, string id, string managementSchema, string dataSchema, int databaseCommandTimeout
            _dotStatDbMock = new Mock<SqlDotStatDb>(
                Configuration.SpacesInternal.FirstOrDefault(), new Version(0,0));


            _unitOfWorkMock.Setup(x => x.ArtefactRepository.FillMeta(
                It.IsAny<Dsd>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.CompletedTask);

            _unitOfWorkMock.Setup(x => x.CodeListRepository.GetDimensionCodesFromDb(
                    It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(new string[0]));


            _unitOfWorkMock.Setup(x => x.DataStoreRepository.CopyDataToNewVersion(
                    It.IsAny<Dsd>(), It.IsAny<DbTableVersion>(), It.IsAny<DbTableVersion>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            
            _unitOfWorkMock.Setup(x => x.DataStoreRepository.CopyAttributesToNewVersion(
                    It.IsAny<Dsd>(), It.IsAny<DbTableVersion>(), It.IsAny<DbTableVersion>(), It.IsAny<CancellationToken>()))
                .Returns(null);
            
            _unitOfWorkMock.Setup(x => x.DataStoreRepository.BulkInsertData(It.IsAny<IAsyncEnumerable<ObservationRow>>(),
                It.IsAny<ReportedComponents>(),
                It.IsAny<CodeTranslator>(), It.IsAny<Dataflow>(),
                _param.Object.ValidationType == ValidationType.ImportWithFullValidation,
                It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(new BulkImportResult(new List<DataSetAttributeRow>(), new List<IValidationError>(), 0, new Dictionary<int, BatchAction>())));

            var importSummary = new ImportSummary()
            {
                ObservationsCount = 100,
                ObservationLevelMergeResult = new MergeResult(0, 100, 0, new List<IValidationError>()),
                SeriesLevelMergeResult = new MergeResult(0, 0, 0, new List<IValidationError>()),
                DataFlowLevelMergeResult = new MergeResult(0, 0, 0, new List<IValidationError>()),
            };

            _unitOfWorkMock.Setup(x => x.DataStoreRepository.MergeStagingTable(
                    It.IsAny<IImportReferenceableStructure>(), It.IsAny<ReportedComponents>(),
                    It.IsAny<Dictionary<int, BatchAction>>(), It.IsAny<List<DataSetAttributeRow>>(), It.IsAny<CodeTranslator>(),
                    It.IsAny<DbTableVersion>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(importSummary));

            _unitOfWorkMock.Setup(x => x.TransactionRepository.MarkTransactionReadyForValidation(
                    It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(true));

            if (_referencedStructureType == ReferencedStructureType.Dsd)
            {
                _referencedStructure = base.GetDsd();
                _mappingStore.Setup(x =>
                        x.GetDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                            It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()))
                    .Returns(_referencedStructure.Dsd);
            }
            else
            {
                _referencedStructure = base.GetDataflow();
                _mappingStore.Setup(x =>
                        x.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                            It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()))
                    .Returns(_referencedStructure as Dataflow);
            }

            _param.Setup(exp => exp.Principal)
                .Returns(new DotStatPrincipal(new ClaimsPrincipal(), new Dictionary<string, string>()));

            _param.Setup(exp => exp.DestinationDataspace).Returns(new DataspaceInternal {Id = dataSpaceId});
            _param.Setup(exp => exp.DestinationReferencedStructure).Returns(new IdentifiableReferencedStructure{ AgencyId = _referencedStructure.AgencyId, Id = _referencedStructure.Code, Version = _referencedStructure.Version.ToString(), Type = _referencedStructureType});
            _param.Setup(exp => exp.CultureInfo).Returns(CultureInfo.CurrentUICulture);

            _dotStatDbServiceMock.Setup(x => 
                    x.TryNewTransaction(
                        It.IsAny<Transaction>(), 
                        It.IsAny<IImportReferenceableStructure>(),
                        It.IsAny<IMappingStoreDataAccess>()
                    , It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(true));

            _dotStatDbServiceMock.Setup(x => x.DeleteMetadata(
                    It.IsAny<Dsd>(), It.IsAny<DbTableVersion>(), It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            _dotStatDbServiceMock.Setup(x => x.CopyMetadataToNewVersion(
                    It.IsAny<Dsd>(), It.IsAny<DbTableVersion>(), It.IsAny<DbTableVersion>(), It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>()))
                .Returns(null);

            _authorisation.Setup(x => x.IsAuthorized(
                It.IsAny<DotStatPrincipal>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<PermissionType>())
            ).Returns(true);

            _sqlDatasetAttributeDatabaseValidator.Setup(x => x.Validate(
                It.IsAny<DotStatDbBase<DbConnection>>(),
                It.IsAny<Dataflow>(),
                It.IsAny<DbTableVersion>(),
                It.IsAny<List<Domain.Attribute>>(),
                It.IsAny<IList<DataSetAttributeRow>>(),
                It.IsAny<int>(), 
                It.IsAny<bool>(),
                It.IsAny<CancellationToken>())).Returns(Task.FromResult(true));

            _sqlDatasetAttributeDatabaseValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());
            _datasetAttributeValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());
            _sqlDatabaseValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());
            
            _content = new TransferContent()
            {
                DataObservations = ObservationGenerator.Generate(_referencedStructure.Dsd, _referencedStructure as Dataflow, true, 2010, 2020, 100),
                DatasetAttributes = new List<DataSetAttributeRow>(),
                ReportedComponents = new ReportedComponents
                {
                    DatasetAttributes = _referencedStructure.Dsd.Attributes
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList(),

                    //Attributes not attached to the time dimension
                    SeriesAttributesWithNoTimeDim = _referencedStructure.Dsd.Attributes
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                        .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                        .ToList(),

                    //Attributes not attached to the time dimension
                    ObservationAttributes = _referencedStructure.Dsd.Attributes
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                        || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList(),
                    Dimensions = _referencedStructure.Dsd.Dimensions.ToList(),
                    TimeDimension = _referencedStructure.Dsd.TimeDimension,
                    IsPrimaryMeasureReported = true
                }
            };

            DotStatDbResolver dotStatDbResolver = dataSpaceId => _dotStatDbMock.Object;
            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => _unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => _dotStatDbServiceMock.Object;

            _sqlConsumer = new SqlConsumer(
                _authorisation.Object,
                _mappingStore.Object,
                this.Configuration,
                dotStatDbResolver,
                unitOfWorkResolver,
                dotStatDbServiceResolver,
                _sqlDatasetAttributeDatabaseValidator.Object,
                _datasetAttributeValidator.Object,
                _sqlDatabaseValidator.Object
                );
        }

        [Test]
        public async Task SaveLiveVersionData()
        {
            _dotStatDbServiceMock.Setup(x => 
                x.TryNewTransaction(It.IsAny<Transaction>(),
                    It.IsAny<IImportReferenceableStructure>(),
                    It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(true));

            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.Live);

            if (_referencedStructureType == ReferencedStructureType.Dsd && _transferType is TransferType.DataAndMetadata or TransferType.DataOnly)
            {
                var exe = Assert.ThrowsAsync<DotStatException>(() => _sqlConsumer.Save(
                    _param.Object,
                    new Transaction { FinalTargetVersion = _param.Object.TargetVersion },
                    _referencedStructure,
                    _content,
                    new CancellationToken()
                ));
                Assert.AreEqual(exe.Message, LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdReferenceNotSupported));
                return;
            }

            bool success = await _sqlConsumer.Save(
                _param.Object,
                new Transaction { FinalTargetVersion = _param.Object.TargetVersion },
                _referencedStructure,
                _content,
                new CancellationToken()
                );

            //_authorisation.Verify(mock => mock.IsAuthorized(
            //                It.IsAny<DotStatPrincipal>(),
            //                "DummySpace",
            //                _referencedStructure.AgencyId,
            //                _referencedStructure.Code,
            //                _referencedStructure.Version.ToString(),
            //                PermissionType.CanImportData
            //            ),
            //            Times.Once
            //        );
     
            _dotStatDbServiceMock.Verify(m =>
                m.TryNewTransaction(It.IsAny<Transaction>(),
                    It.IsAny<IImportReferenceableStructure>(),
                    It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()));

            //Check that  live version was called and not PIT
            _dotStatDbServiceMock.Verify(
                m => m.ApplyPITRelease(It.IsAny<IImportReferenceableStructure>(), It.IsAny<bool>(),
                        It.IsAny<IMappingStoreDataAccess>()), Times.Never);

           Assert.IsTrue(success);
        }
        
        [Test]
        public async Task ExecutePitRelease()
        {
            _referencedStructure.Dsd.LiveVersion = (char)DbTableVersion.A;
            _referencedStructure.Dsd.PITReleaseDate = DateTime.MinValue;
            _referencedStructure.Dsd.PITVersion = (char)DbTableVersion.B;

            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.PointInTime);

            if (_referencedStructureType == ReferencedStructureType.Dsd && _transferType is TransferType.DataAndMetadata or TransferType.DataOnly)
            {
                var exe = Assert.ThrowsAsync<DotStatException>(() => _sqlConsumer.Save(
                    _param.Object,
                    new Transaction { FinalTargetVersion = _param.Object.TargetVersion },
                    _referencedStructure,
                    _content,
                    new CancellationToken()
                ));
                Assert.AreEqual(exe.Message, LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdReferenceNotSupported));
                return;
            } 

            var success = await _sqlConsumer.Save(
                _param.Object,
                new Transaction { FinalTargetVersion = _param.Object.TargetVersion },
                _referencedStructure,
                _content,
                new CancellationToken()
            );
            
            _dotStatDbServiceMock.Verify(
                m => m.ApplyPITRelease(
                    It.IsAny<IImportReferenceableStructure>(), It.IsAny<bool>(),
                    It.IsAny<IMappingStoreDataAccess>()), Times.Once);

            Assert.IsTrue(success);
        }

        [Test]
        public async Task CopyDataToPitVersion()
        {
            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.PointInTime);

            if (_referencedStructureType == ReferencedStructureType.Dsd && _transferType is TransferType.DataAndMetadata or TransferType.DataOnly)
            {
                var exe = Assert.ThrowsAsync<DotStatException>(() => _sqlConsumer.Save(
                    _param.Object,
                    new Transaction { FinalTargetVersion = _param.Object.TargetVersion },
                    _referencedStructure,
                    _content,
                    new CancellationToken()
                ));
                Assert.AreEqual(exe.Message, LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdReferenceNotSupported));
                return;
            }

            bool success = await _sqlConsumer.Save(
                _param.Object,
                new Transaction { FinalTargetVersion = _param.Object.TargetVersion},
                _referencedStructure,
                _content,
                new CancellationToken()
            );

            _unitOfWorkMock.Verify(
                m => m.DataStoreRepository.CopyAttributesToNewVersion(It.IsAny<Dsd>(), It.IsAny<DbTableVersion>(), It.IsAny<DbTableVersion>(), It.IsAny<CancellationToken>()), Times.Once);

            Assert.IsTrue(success);
        }
        
        [Test]
        public async Task GetReferencedStructure()
        {
            var referencedStructure = await _sqlConsumer.GetReferencedStructure(_param.Object);

            if( _referencedStructureType == ReferencedStructureType.DataFlow )
                _mappingStore.Verify(x=> x.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()), Times.Once);
            else 
                _mappingStore.Verify(x => x.GetDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()), Times.Once);

            Assert.AreEqual(_referencedStructure.FullId, referencedStructure.FullId);

        }

        [TearDown]
        public void TearDown()
        {
            _authorisation.Invocations.Clear();
            _metadataStoreRepository.Invocations.Clear();
            _mappingStore.Invocations.Clear();
            _dotStatDbMock.Invocations.Clear();
            _unitOfWorkMock.Invocations.Clear();
            _dotStatDbServiceMock.Invocations.Clear();
        }
    }
}
