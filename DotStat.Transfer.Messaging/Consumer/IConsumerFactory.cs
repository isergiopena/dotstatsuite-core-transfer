﻿using System.Net.Http;
using DotStat.Db.Manager;
using DotStat.MappingStore;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using Microsoft.Extensions.Configuration;

namespace DotStat.Transfer.Messaging.Consumer
{
    public interface IConsumerFactory
    {
        IConsumer Create(IConfiguration configuration, CommonManager commonMngr, IDbManager dbManager, IMappingStoreDataAccess mappingStoreDataAccess, IMailService mailService);
    }

    
}
