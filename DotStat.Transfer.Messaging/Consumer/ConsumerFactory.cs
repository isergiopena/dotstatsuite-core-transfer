﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Db.Manager;
using DotStat.MappingStore;
using DotStat.Transfer.Manager;
using DotStat.Common.Messaging;
using DotStat.Transfer.Interface;
using Microsoft.Extensions.Configuration;

namespace DotStat.Transfer.Messaging.Consumer
{
    [ExcludeFromCodeCoverage]
    public class ConsumerFactory : IConsumerFactory
    { 
        public IConsumer Create(IConfiguration configuration, CommonManager commonMngr, IDbManager dbManager, IMappingStoreDataAccess mappingStoreDataAccess, IMailService mailService)
        {
            MessagingServiceConfiguration messsageBrokerConfiguration = configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();
            
            try
            {
                if (messsageBrokerConfiguration?.Enabled == true)
                    return new RabbitMqConsumer(configuration, commonMngr, dbManager, mappingStoreDataAccess, mailService);
            }
            catch
            {
                //Catch without action to let NSIWS continue without crashing
            }

            return null;
        }
    }
}
