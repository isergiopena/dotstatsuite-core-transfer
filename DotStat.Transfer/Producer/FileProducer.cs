﻿using DotStat.Common.Auth;
using DotStat.MappingStore;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Producer
{
    public abstract class FileProducer<T> : BaseFileProducer<T> where T : TransferParam, ITransferParamWithFilePath
    {
        protected FileProducer(IAuthorizationManagement authorizationManagement, IMappingStoreDataAccess dataAccess, ITempFileManagerBase tempFileManager)
            : base(authorizationManagement, dataAccess, tempFileManager)
        {
        }
    }
}
