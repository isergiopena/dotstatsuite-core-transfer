﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DotStat.Transfer.Producer
{
    public abstract class UrlProducer<T> : BaseFileProducer<T> where T : TransferParam, ITransferParamWithUrl
    {
        private readonly IGeneralConfiguration _generalConfiguration;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ITempFileManagerBase _tempFileManager;

        protected UrlProducer(IAuthorizationManagement authorizationManagement, ITempFileManagerBase tempFileManager, IHttpClientFactory httpClientFactory, IGeneralConfiguration generalConfiguration, IMappingStoreDataAccess dataAccess)
            : base(authorizationManagement, dataAccess, tempFileManager)
        {
            _httpClientFactory = httpClientFactory;
            _generalConfiguration = generalConfiguration;
            _tempFileManager = tempFileManager;
        }

        protected override async Task GetFileFromSource(T transferParam)
        {
            var fileName = await DownloadFile(transferParam);

            transferParam.FilesToDelete.Add(fileName);
            transferParam.FilePath = fileName;

            await base.GetFileFromSource(transferParam);
        }

        private async Task<string> DownloadFile(T transferParam)
        {
            try
            {
                var httpClient = _httpClientFactory.CreateClient();
                httpClient.Timeout = TimeSpan.FromSeconds(_generalConfiguration.HttpClientTimeOut);

                if (transferParam.AuthenticateToRemoteUrl)
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", transferParam.Token);
                }

                var response = await httpClient.GetAsync(transferParam.Url, HttpCompletionOption.ResponseHeadersRead);
                response.EnsureSuccessStatusCode();

                var fileName = _tempFileManager.GetTempFileName();

                await using var stream = await response.Content.ReadAsStreamAsync();
                await using var fileStream = File.Open(fileName, FileMode.Create);
                await stream.CopyToAsync(fileStream);

                return fileName;
            }
            catch (TaskCanceledException)
            {
                throw new TransferFailedException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.HttpClientTimeOut), transferParam.Url, _generalConfiguration.HttpClientTimeOut));
            }
            catch (HttpRequestException)
            {
                throw new DotStatException(message: string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SdmxSourceHttpRequestException), transferParam.Url));
            }
        }
    }
}
