﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Transfer.Utils
{
    public delegate Task<bool> CanBeProcessed(CancellationToken cancellationToken);

    public class TransactionQueueItem
    {
        public readonly CanBeProcessed CanBeProcessed;
        public readonly Func<CancellationToken, Task> MainTask;
        public readonly Func<CancellationToken, Task> NotifyInProgressTask;
        public readonly Action CallbackAction;
        private int _trialNum = 0;


        public TransactionQueueItem(CanBeProcessed canBeProcessed, Func<CancellationToken, Task> mainTask, Func<CancellationToken, Task> notifyInProgressTask, Action callbackAction)
        {
            CanBeProcessed = canBeProcessed;
            MainTask = mainTask;
            NotifyInProgressTask = notifyInProgressTask;
            CallbackAction = callbackAction;
        }
        
        public async Task DelayRequeue(CancellationToken cancellationToken)
        {
            var waitTime = ++_trialNum * 5000;

            //Set a random delay to minimize the probabillity for a collition of two requests reaching the transaction lock at the same time.
            var randomDelay = new Random().Next(waitTime, waitTime * 2);

            // Delay the requeuing of this request.
            await Task.Delay(randomDelay, cancellationToken);
        }
    }
}
