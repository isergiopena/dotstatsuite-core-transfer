﻿using DotStat.Common.Logger;
using DotStat.Domain;
using log4net.Core;

namespace DotStat.Transfer.Interface
{

    public interface ITransactionResult
    {

        DotStat.Domain.TransactionType TransactionType { get; set; }
        public int TransactionId { get; set; }
        public string SourceDataSpaceId { get; set; }
        public string DestinationDataSpaceId { get; set; }
        public string Aftefact { get; set; }
        public string User { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public string DataSource { get; set; }

        public LoggingEvent[] TransactionLogs => LogHelper.GetRecordedEvents(TransactionId, DestinationDataSpaceId);

    }
}