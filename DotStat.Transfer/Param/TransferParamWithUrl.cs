﻿using DotStat.Common.Configuration.Dto;
using DotStat.Transfer.Model;
using Org.Sdmxsource.Util.Url;
using System;
using Microsoft.AspNetCore.Http;

namespace DotStat.Transfer.Param
{
    public class TransferParamWithUrl : TransferParamWithFilePath, ITransferParamWithUrl
    {
        public Uri Url { get; set; }
        public bool AuthenticateToRemoteUrl { get; set; }
        public string Token { get; set; }

        public TransferParamWithUrl(InternalSdmxParameters internalSdmxParameters, DataspaceInternal destinationDataspace, string language, IHeaderDictionary headerDictionary) 
            : base(internalSdmxParameters, destinationDataspace, language)
        {
            Url = internalSdmxParameters.filepath.ToUri();

            if (internalSdmxParameters.authenticateToRemoteURL.HasValue &&
                internalSdmxParameters.authenticateToRemoteURL.Value &&
                headerDictionary.ContainsKey("Authorization"))
            {
                AuthenticateToRemoteUrl = true;
                Token = headerDictionary["Authorization"].ToString().Replace("Bearer ", String.Empty, StringComparison.OrdinalIgnoreCase);
            }
        }
    }
}
