﻿using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Query;

namespace DotStat.Transfer.Param
{
    public interface ISqlTransferParam : ITransferParam 
    {
        IdentifiableReferencedStructure SourceReferencedStructure { get; }
        IRestDataQuery SourceQuery { get; }

        IdentifiableReferencedStructure DestinationReferencedStructure { get; set; }
    }

    public interface IFromSqlTransferParam : ISqlTransferParam
    {
        TargetVersion SourceVersion { get; set; }
    }

    public class SqlToSqlTransferParam : TransferParam, IFromSqlTransferParam
    {
        public IdentifiableReferencedStructure SourceReferencedStructure { get; set; }
        public IdentifiableReferencedStructure DestinationReferencedStructure { get; set; }
        public IRestDataQuery SourceQuery { get; set; }

        public TargetVersion SourceVersion { get; set; }
    }

    public class IdentifiableReferencedStructure
    {
        public string AgencyId { get; set; }
        public string Id { get; set; }
        public string Version { get; set; }
        public string FullId => $"{AgencyId}:{Id}({Version})";
        public ReferencedStructureType Type { get; set; }
}

    public enum ReferencedStructureType
    {
        Dsd,
        DataFlow
    }
}
