﻿
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Exception
{
    public class TransferFailedException : DotStatException
    {
        [ExcludeFromCodeCoverage]
        public TransferFailedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferFailedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferFailedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
