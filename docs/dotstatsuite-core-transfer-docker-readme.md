# About this Image

The official transfer service image of SIS-CC .Stat Suite, configured to be ready for using in a [.Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/) installation.

Source repository: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer

# Environment Variables
You can use environment variables to configure the dotstatsuite-core-transfer service.  
For a complete list of supported environment variables, please see the configuration section of the documentation: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/blob/develop/README.md?ref_type=heads#configuration

# How to use this image
Start dotstatsuite-core-transfer instance using the latest release. 

IMPORTANT NOTE: If you are using PowerShell on Windows to run these commands use double quotes instead of single quotes.

Example:
```
docker run \
-e 'MaxTransferErrorAmount=0' \
-e 'DefaultLanguageCode=en' \
-e 'SmtpHost=smtp.gmail.com' \
-e 'SmtpPort=465' \
-e 'SmtpEnableSsl=true' \
-e 'SmtpUserName=user@gmail.com' \
-e 'SmtpUserPassword=password' \
-e 'MailFrom=transfer-service@gmail.com' \
-e 'DotStatSuiteCoreCommonDbConnectionString=Server=db;Database=CommonDb;User=sa;Password=yourStrong(!)Password;' \
-e 'SpacesInternal__0__DotStatSuiteCoreStructDbConnectionString=Server=db;Database=designStructDb;User=sa;Password=yourStrong(!)Password;' \
-e 'SpacesInternal__0__DotStatSuiteCoreDataDbConnectionString=Server=db;Database=designDataDb;User=sa;Password=yourStrong(!)Password;' \
-e 'SpacesInternal__0__DataImportTimeOutInMinutes=60' \
-e 'SpacesInternal__0__DatabaseCommandTimeoutInSec=60' \
-p 93:80 \
siscc/dotstatsuite-core-transfer:master
```
