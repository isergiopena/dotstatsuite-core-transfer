﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using DotStat.Common.Exceptions;
using DotStat.Common.Logger;
using DotStat.Common.Messaging;
using Estat.Sdmxsource.Extension.Model.Error;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace DotStat.Transfer.Messaging.Producer
{
    public class RabbitMqProducer : IProducer, IDisposable
    {
        private readonly MessagingServiceConfiguration _msgBrokerConf;

        //Create global connection since its recommended to keep connection and channel open, https://www.rabbitmq.com/dotnet-api-guide.html#connection-and-channel-lifspan
        //Abstract IConnection to be generic, not RabbitMQ dependent 
        private readonly IConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;
        private static string _dataspace;

        public RabbitMqProducer(IConfiguration configuration, IConnectionFactory connectionFactory)
        {
            try
            {
                _msgBrokerConf = configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();

                if (_msgBrokerConf?.Enabled != true) 
                    return;
                
                _dataspace = configuration.GetSection("mappingStore:Id").GetValue<string>("Default");

                _connectionFactory = connectionFactory ??= new ConnectionFactory
                {
                    HostName = _msgBrokerConf.HostName,
                    VirtualHost = _msgBrokerConf.VirtualHost,
                    Port = _msgBrokerConf.Port,
                    UserName = _msgBrokerConf.UserName,
                    Password = _msgBrokerConf.Password
                };
                
                CreateConnection();
                
            }
            catch (Exception e)
            {
                Log.Fatal(e);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="principal"></param>
        /// <returns></returns>
        public void Notify(IList<IResponseWithStatusObject> summary, IPrincipal principal)
        {
            try
            {
                if (_msgBrokerConf?.Enabled != true)
                    return;

                if (_connection == null || _channel == null)
                {
                    CreateConnection();
                }

                var props = _channel.CreateBasicProperties();
                props.DeliveryMode = _msgBrokerConf.DeliveryMode; //Set delivery mode to 2 for persisting queue items
                
                var items = summary.Select(o => 
                    new QueueItem(
                        o.Action.Action,
                        _dataspace,
                        (principal.Identity as ClaimsIdentity)?.FindFirst("name")?.Value, 
                        (principal.Identity as ClaimsIdentity)?.FindFirst("email")?.Value,
                        o.StructureReference.MaintainableId,
                        o.StructureReference.AgencyId, 
                        o.StructureReference.Version));
                
                var body = Encoding.UTF8.GetBytes(
                    JsonConvert.SerializeObject(items)
                );

                _channel.BasicPublish(
                    exchange: "", //use default exchange
                    routingKey: _msgBrokerConf.QueueName,
                    basicProperties: props,
                    body: body);
                
            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
            }
            catch (Exception exception)
            {
                Log.Fatal(exception);
            }
        }
    

        public void Dispose()
        {
            if (_connection?.IsOpen == true)
                _connection.Close(); //Closes connection and all its channels
        }

        /// <summary>
        /// Creates connection to the messagebroker
        /// </summary>
        private void CreateConnection()
        {
            //Try to connect 5 times if rabbitmq is in starting up phase
            for (var i = 0; i <= 5; i++)
            {
                try
                {
                    _connection = _connectionFactory.CreateConnection();
                    _channel = _connection.CreateModel();
                    _channel.QueueDeclare(
                        queue: _msgBrokerConf.QueueName,
                        durable: _msgBrokerConf.QueueDurable,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    _connection.ConnectionShutdown += (sender, ea) =>
                    {
                        //if messagebroker is unreachable, try to restart 
                        CreateConnection();
                    };

                    break;
                }
                catch (Exception e) when (e is BrokerUnreachableException || e is OperationInterruptedException)
                {
                    if (i < 5)
                        Thread.Sleep(TimeSpan.FromSeconds(10));
                    else
                        Log.Fatal(e);
                    
                }
            }

        }

    }
}
