﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Messaging;

namespace DotStat.Transfer.Messaging.Producer
{

    [ExcludeFromCodeCoverage]
    public static class ProducerFactory
    {
        ///Todo: abstract IConnectionFactory, currently its part of the RabbitMq namespace but should be generic for other messagebroker vendors 
        public static IProducer Create(IConfiguration configuration, IConnectionFactory connectionFactory = null)
        {
            var conf = configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();

            return conf?.Enabled == true ? new RabbitMqProducer(configuration, connectionFactory) : null;
        }
    }
}
