﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Auth;
using DotStat.Common.Model;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStatServices.Transfer
{

    /// <summary>
    /// Used in DI when authentication is disabled in app configuration
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class NoAuthorizationManagement : IAuthorizationManagement
    {
        public bool IsAuthorized(DotStatPrincipal principal, PermissionType requestedPermission)
        {
            return true;
        }

        public bool IsAuthorized(DotStatPrincipal principal, string dataSpace, string artefactAgencyId, string artefactId, string artefactVersion, PermissionType requestedPermission)
        {
            return true;
        }

        public IEnumerable<UserAuthorization> AllRules()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserAuthorization> UserRules(DotStatPrincipal principal)
        {
            throw new NotImplementedException();
        }

        public int SaveAuthorizationRule(UserAuthorization ua)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAuthorizationRule(int id)
        {
            throw new NotImplementedException();
        }

        public UserAuthorization GetAuthorizationRule(int id)
        {
            throw new NotImplementedException();
        }
    }
}