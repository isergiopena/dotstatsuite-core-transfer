﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotStatServices.Transfer.HealthCheck
{
    [ExcludeFromCodeCoverage]
    public class DiskSizeHealthCheck : IHealthCheck
    {
        private IGeneralConfiguration _configuration;

        public DiskSizeHealthCheck(IGeneralConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            var data = new Dictionary<string, object>()
            {
                { "tempPath", _configuration.TempFileDirectory },
            };

            var dirInfo = new DirectoryInfo(_configuration.TempFileDirectory);

            if (!dirInfo.Exists)
                return HealthCheckResult.Unhealthy(data:data);
            
            var driveInfo = new DriveInfo(_configuration.TempFileDirectory);
            
            data.Add("totalSize", driveInfo.TotalSize.ToFileSize());
            data.Add("totalFreeSpace", driveInfo.TotalFreeSpace.ToFileSize());
            data.Add("availableFreeSpace", driveInfo.AvailableFreeSpace.ToFileSize());
            data.Add("free", $"{100m * driveInfo.AvailableFreeSpace / driveInfo.TotalSize:0.0}%");
            data.Add("filesCount", dirInfo.EnumerateFiles().Count());

            return HealthCheckResult.Healthy(data: data);
        }
    }

    internal static class Extensions
    {
        public static string ToFileSize(this long bytes)
        {
            var unit = 1024;
            if (bytes < unit) { return $"{bytes} B"; }

            var exp = (int)(Math.Log(bytes) / Math.Log(unit));
            return $"{bytes / Math.Pow(unit, exp):F2} {("KMGTPE")[exp - 1]}B";
        }
    }
}
