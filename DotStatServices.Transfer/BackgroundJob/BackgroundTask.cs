﻿using System;
using DotStat.Common.Logger;
using DotStat.Transfer.Messaging.Consumer;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace DotStatServices.Transfer.BackgroundJob
{
    internal static class BackgroundTask
    {
        // Background Hosted service for Import & Transfer tasks 
        // https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/multi-container-microservice-net-applications/background-tasks-with-ihostedservice
        public static void Register(IServiceCollection services, int maxConcurrentRequests)
        {
            if (maxConcurrentRequests <= 0)
            {
                Log.Warn($"The MaxConcurrentRequests configuration setting must be a positive number (configured value={maxConcurrentRequests}). Changing the value to '1'. ");
                maxConcurrentRequests = 1;
            }

            services.AddSingleton(new BackgroundQueue(OnError, maxConcurrentRequests, 500));
            services.AddSingleton<IHostedService, BackgroundQueueService>();
        }

        private static void OnError(Exception ex)
        {
            Log.Error(ex);
        }
    }
}
