using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DotStat.Domain;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Mapping
{
    public sealed class NamedMappingItemList
    {
        //compare only for codes
        private sealed class MappingItemListEqualityComparer : IEqualityComparer<MappingItem>
        {
            public bool Equals(MappingItem x, MappingItem y)
            {
                return x.ExternMember.IsSameAs(y.ExternMember)
                       && x.SWMember.IsSameAs(y.SWMember);
            }

            public int GetHashCode(MappingItem mi)
            {
                unchecked
                {
                    int res = 7;
                    res = (res + (mi.SWMember != null ? mi.SWMember.GetHashCode() : 0))*31;
                    res = (res + (mi.ExternMember != null ? mi.ExternMember.GetHashCode() : 7))*31;
                    return res;
                }
            }
        }

        public struct MappingItem
        {
            public readonly int SWMemberId;
            public readonly string SWMember;
            public readonly string ExternMember;
            public readonly bool IsRegex;

            public MappingItem(int memId, string mem, string extMember, bool isRegex)
            {
                SWMemberId = memId;
                SWMember = mem;
                SWMemberId = memId;
                ExternMember = extMember;
                IsRegex = isRegex;
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int res = 7;
                    res = (res + (SWMember != null ? SWMember.GetHashCode() : 0))*31;
                    res = (res + (ExternMember != null ? ExternMember.GetHashCode() : 7))*31;
                    res = (res + IsRegex.GetHashCode())*31;
                    return res;
                }
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return false;
                }
                if (!(obj is MappingItem))
                {
                    return false;
                }
                var mi = (MappingItem)obj;
                return IsRegex == mi.IsRegex
                       && ExternMember.IsSameAs(mi.ExternMember)
                       && SWMember.IsSameAs(mi.SWMember);
            }
        }

        private readonly Dimension _Dimension;
        public readonly string Name;
        public readonly IEnumerable<MappingItem> Mappings;
        private readonly IndexedSet<MappingItem> _MappingItems;

        public NamedMappingItemList(Dimension dimension, string name)
        {
            _Dimension = dimension;
            Name = name;
            _MappingItems = new IndexedSet<MappingItem>(new MappingItemListEqualityComparer());
            Mappings = _MappingItems;
        }

        public bool ContainsMappingFor(string sourceMember)
        {
            return _MappingItems.Any(mi => mi.ExternMember.IsSameAs(sourceMember));
        }

        public void AddMapping(string externMember, string member, bool isRegex = false)
        {
            throw new NotImplementedException();

            //var mid = 0;
            //if (_Dimension != null)
            //{
            //    var mem = _Dimension.FindMember(member);
            //    if (mem != null)
            //    {
            //        mid = mem.DbId;
            //    }
            //}
            //var mi = new MappingItem(mid, member, externMember, isRegex);
            //if (_MappingItems.Contains(mi))
            //{
            //    throw new ApplicationArgumentException("A mapping has already been added for {0}".F(externMember));
            //}
            //_MappingItems.Add(mi);
        }

        public void AddOrReplaceMapping(string externMember, string member, bool isRegex = false)
        {
            throw new NotImplementedException();

            //var mid = 0;
            //if (_Dimension != null)
            //{
            //    var mem = _Dimension.FindMember(member);
            //    if (mem != null)
            //    {
            //        mid = mem.DbId;
            //    }
            //}
            //var mi = new MappingItem(mid, member, externMember, isRegex);
            //_MappingItems.AddOrReplace(mi);
        }

        public string Transform(string mt)
        {
            var res = _MappingItems.IndexOf(mi => !mi.IsRegex && mt.IsSameAs(mi.ExternMember));
            if (res < 0)
            {
                res =
                    _MappingItems.IndexOf(
                        mi =>
                            mi.IsRegex &&
                            Regex.IsMatch(mt, mi.ExternMember, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase));
            }
            return res >= 0
                ? _MappingItems[res].SWMember
                : mt;
        }
    }

    public sealed class ExternDimensionSWDimensionPair
    {
        public readonly IEnumerable<NamedMappingItemList.MappingItem> Mappings;
        private readonly NamedMappingItemList _NamedMappings;

        public readonly Dimension SWDimension;
        public readonly string Dimension;
        public readonly string ExternDimension;
        public readonly string FixedMember;
        public readonly string FixedExternMember;
        public readonly string MappingItemRefName;

        public ExternDimensionSWDimensionPair(
            Dimension dim,
            string swdim,
            string externDim,
            string fixedMember = null,
            string fixedExternMember = null,
            string mappingItemListName = null) : this(dim != null
                ? dim.Code
                : swdim,
                externDim,
                fixedMember,
                fixedExternMember,
                mappingItemListName)
        {
            SWDimension = dim;
            _NamedMappings = new NamedMappingItemList(SWDimension, "internal");

            Mappings = _NamedMappings.Mappings;
        }

        private ExternDimensionSWDimensionPair(
            string dim,
            string externDim,
            string fixedMember = null,
            string fixedExternMember = null,
            string mappingItemListName = null)
        {
            //if ( string.IsNullOrEmpty( dim ) ) {
            //    dim = null;
            //}
            //if( string.IsNullOrEmpty( externDim ) ) {
            //    externDim = null;
            //}
            Dimension = dim;
            if (Dimension == null && (fixedExternMember == null || fixedMember != null))
            {
                Dimension = externDim;
            }
            FixedMember = fixedMember;
            ExternDimension = externDim; // ?? dim;
            if (ExternDimension == null && (fixedMember == null || fixedExternMember != null))
            {
                ExternDimension = dim;
            }
            FixedExternMember = fixedExternMember;
            MappingItemRefName = mappingItemListName;
        }

        public void AddMapping(string externMember, string member, bool isRegex = false)
        {
            _NamedMappings.AddMapping(externMember, member, isRegex);
        }

        public void AddOrReplaceMapping(string externMember, string member, bool isRegex = false)
        {
            _NamedMappings.AddOrReplaceMapping(externMember, member, isRegex);
        }

        public override bool Equals(object obj)
        {
            var oth = obj as ExternDimensionSWDimensionPair;
            if (oth == null)
            {
                return false;
            }

            if (!Dimension.IsSameAs(oth.Dimension)
                || !FixedMember.IsSameAs(oth.FixedMember)
                || !FixedExternMember.IsSameAs(oth.FixedExternMember)
                || !ExternDimension.IsSameAs(oth.ExternDimension))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var res = 7;
                res = (res + (Dimension == null ? 0 : Dimension.GetHashCode()))*31;
                res = (res + (FixedMember == null ? 0 : FixedMember.GetHashCode()))*31;
                res = (res + (FixedExternMember == null ? 0 : FixedExternMember.GetHashCode()))*31;
                res = (res + (ExternDimension == null ? 0 : ExternDimension.ToLowerInvariant().GetHashCode()))*31;

                return res;
            }
        }

        public bool ContainsMappingFor(string sourceMember)
        {
            return FixedMember != null || _NamedMappings.ContainsMappingFor(sourceMember);
        }
    }
}