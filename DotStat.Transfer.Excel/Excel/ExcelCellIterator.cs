﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Localization;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Excel
{
    public abstract class ExcelCellIterator : IRecordIterator
    {
        protected struct CellValue
        {
            public string Value;
            public string Source;
        }

        internal readonly IExcelDataSource DataSource;
        internal readonly string SheetName;

        protected int _CurrentRecordNo;
        protected CellReference _Current;

        protected readonly Dictionary<string, int> _FieldsByName = new Dictionary<string, int>(StringComparer.InvariantCultureIgnoreCase);
        protected readonly Dictionary<int, string> _FieldsById = new Dictionary<int, string>();

        //this is used for exporting dimension member records
        public Observation TargetRecord;

        protected ExcelCellIterator(IExcelDataSource dataSource, string sheetName)
        {
            SheetName = sheetName;
            DataSource = dataSource;
        }

        internal CellReference Current => _Current;
        public int CurrentRecordNo => _CurrentRecordNo;
        public virtual int FieldCount => _FieldsByName.Count;
        public abstract string Name { get; }

        public virtual int StartRow { get; }
        public virtual int StartColumn { get; }
        public virtual int EndRow { get; }
        public virtual int EndColumn { get; }

        public abstract SWMapper Mapper { get; }

        protected abstract CellReference GetNextDataCell();

        public abstract string GetField(int pos);

        public virtual string GetFieldName(int pos)
        {
            string res;
            if (_FieldsById.TryGetValue(pos, out res))
            {
                return res;
            }
            return null;
        }

        /// <param name="field"></param>
        /// <returns></returns>
        public virtual int GetFieldIndex(string field)
        {
            int res;
            if (_FieldsByName.TryGetValue(field, out res))
            {
                return res;
            }
            return -1;
        }

        public virtual bool Read()
        {
            if (!DataSource.CurrentWorksheet.IsSameAs(SheetName))
            {
                DataSource.CurrentWorksheet = SheetName;
            }

            do
            {
                var nc = GetNextDataCell();
                ResetCurrentDataExpressions();

                if (nc == null)
                    return false;

                _Current = nc;

                if (!IsCellExcluded())
                    break;
            }
            while (true);

            _CurrentRecordNo++;
            return true;
        }

        public void Dispose()
        {
            //noop
        }

        public virtual void Reset()
        {
            ResetCurrentDataExpressions(true);

            _CurrentRecordNo = 0;
            _Current = null;
        }

        protected virtual void ResetCurrentDataExpressions(bool resetOwner = false)
        {}

        protected virtual bool IsCellExcluded()
        {
            return false;
        }

        public bool IsError
        {
            get { return !DataSource.IsCellValueValid(_Current.Row, _Current.Column, SheetName); }
        }
    }

    public abstract class ExcelCellRangeIterator : ExcelCellIterator
    {
        private int _startRow;
        private int _startColumn;
        private int _endRow;
        private int _endColumn;

        private bool _skipEmpty;
        private CellValue[] _currentCoord;

        private IEnumerable<IEnumerable<int>> _rowColCombinations;
        private IEnumerator<IEnumerable<int>> _enumerator;

        private HashSet<int> _evalExcludedRows;
        private HashSet<int> _evalExcludedColumns;
        private HashSet<ulong> _evalExcludedCells;

        protected virtual ExcelCellRangeDescriptor CoordDescriptor { get;}

        protected ExcelCellRangeIterator(ExcelCellRangeDescriptor coordDescriptor, IExcelDataSource dataSource, string sheetName, bool skipEmpty = false) : base(dataSource, sheetName)
        {
            CoordDescriptor = coordDescriptor;
            _skipEmpty = skipEmpty;
        }

        public override int StartRow
        {
            get
            {
                if (_startRow > 0)
                {
                    return _startRow;
                }
                CoordDescriptor.TopLeft.Owner = this;
                if (CoordDescriptor.TopLeft.CellReferenceResult == null)
                {
                    throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelReaderInvalidCellReferenceInSource),
                        CoordDescriptor.TopLeft.CoordExpression,
                        CoordDescriptor.TopLeft.ExpressionSource)
                    );
                }
                _startRow = CoordDescriptor.TopLeft.CellReferenceResult.Row;
                _startColumn = CoordDescriptor.TopLeft.CellReferenceResult.Column;
                return _startRow;
            }
        }

        public override int StartColumn
        {
            get
            {
                if (_startColumn > 0)
                {
                    return _startColumn;
                }

                CoordDescriptor.TopLeft.Owner = this;
                if (CoordDescriptor.TopLeft.CellReferenceResult == null)
                {
                    throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelReaderInvalidCellReferenceInSource),
                        CoordDescriptor.TopLeft.CoordExpression,
                        CoordDescriptor.TopLeft.ExpressionSource)
                    );
                }

                _startRow = CoordDescriptor.TopLeft.CellReferenceResult.Row;
                _startColumn = CoordDescriptor.TopLeft.CellReferenceResult.Column;
                return _startColumn;
            }
        }

        public override int EndRow
        {
            get
            {
                if (_endRow > 0)
                {
                    return _endRow;
                }
                CoordDescriptor.BottomRight.Owner = this;
                if (CoordDescriptor.BottomRight.CellReferenceResult == null)
                {
                    throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelReaderInvalidCellReferenceInSource),
                        CoordDescriptor.TopLeft.CoordExpression,
                        CoordDescriptor.TopLeft.ExpressionSource)
                    );
                }

                _endRow = CoordDescriptor.BottomRight.CellReferenceResult.Row;
                _endColumn = CoordDescriptor.BottomRight.CellReferenceResult.Column;
                return _endRow;
            }
        }

        public override int EndColumn
        {
            get
            {
                if (_endColumn > 0)
                {
                    return _endColumn;
                }

                CoordDescriptor.BottomRight.Owner = this;
                if (CoordDescriptor.BottomRight.CellReferenceResult == null)
                {
                    throw new UnrecoverableAppException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelReaderInvalidCellReferenceInSource),
                        CoordDescriptor.TopLeft.CoordExpression,
                        CoordDescriptor.TopLeft.ExpressionSource)
                    );
                }
                _endRow = CoordDescriptor.BottomRight.CellReferenceResult.Row;
                _endColumn = CoordDescriptor.BottomRight.CellReferenceResult.Column;
                return _endColumn;
            }
        }

        protected CellValue[] ExternCoordinates => _currentCoord ?? (_currentCoord = GetExternCoordinates());

        protected abstract CellValue[] GetExternCoordinates();

        protected override CellReference GetNextDataCell()
        {
            if (_rowColCombinations == null)
            {
                var cl = BuildColList();
                var rl = BuildRowList();
                _rowColCombinations = (new[] { rl, cl }).CartesianProduct();
                _enumerator = _rowColCombinations.GetEnumerator();
            }
            while (_enumerator.MoveNext())
            {
                var coord   = _enumerator.Current.ToArray();
                var nextCol = coord[1];
                var nextRow = coord[0];

                if (EvalExcludedCells.Contains(((ulong)nextRow << 32) | ((uint)nextCol)))
                    continue;

                var cell = new CellReference(nextRow, nextCol)
                {
                    Owner = this
                };

                if(_skipEmpty && string.IsNullOrEmpty(cell.Value))
                    continue;

                return cell;
            }

            return null;
        }

        private IEnumerable<int> BuildColList()
        {
            for (var ii = StartColumn; ii <= EndColumn; ii++)
            {
                if (!EvalExcludedColumns.Contains(ii))
                {
                    yield return ii;
                }
            }
        }

        private IEnumerable<int> BuildRowList()
        {
            for (var ii = StartRow; ii <= EndRow; ii++)
            {
                if (!EvalExcludedRows.Contains(ii))
                {
                    yield return ii;
                }
            }
        }

        private HashSet<int> EvalExcludedRows
        {
            get
            {
                if (_evalExcludedRows == null)
                {
                    _evalExcludedRows = new HashSet<int>();
                    foreach (var er in CoordDescriptor.ExcludedRows)
                    {
                        er.Owner = this;
                        _evalExcludedRows.AddAll(er.IndexList);
                    }
                }
                return _evalExcludedRows;
            }
        }

        private HashSet<int> EvalExcludedColumns
        {
            get
            {
                if (_evalExcludedColumns == null)
                {
                    _evalExcludedColumns = new HashSet<int>();
                    foreach (var ec in CoordDescriptor.ExcludedColumns)
                    {
                        ec.Owner = this;
                        _evalExcludedColumns.AddAll(ec.IndexList);
                    }
                }
                return _evalExcludedColumns;
            }
        }

        private HashSet<ulong> EvalExcludedCells
        {
            get
            {
                if (_evalExcludedCells == null)
                {
                    _evalExcludedCells = new HashSet<ulong>();
                    foreach (var ec in CoordDescriptor.ExcludedCells)
                    {
                        ec.Owner = this;
                        if (ec.CellReferenceListResult.Count == 0)
                        {
                            throw new UnrecoverableAppException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelReaderInvalidCellExclusion),
                                ec.CoordExpression,
                                ec.ExpressionSource)
                            );
                        }
                        foreach (var cell in ec.CellReferenceListResult)
                        {
                            _evalExcludedCells.Add((((ulong)cell.Row) << 32) | (uint)cell.Column);
                        }
                    }
                }
                return _evalExcludedCells;
            }
        }

        public override void Reset()
        {
            base.Reset();

            _startRow = -1;
            _startColumn = -1;
            _endRow = -1;
            _endColumn = -1;

            _evalExcludedCells = null;
            _evalExcludedColumns = null;
            _evalExcludedRows = null;

            _rowColCombinations = null;
            _enumerator = null;
        }

        protected override void ResetCurrentDataExpressions(bool resetOwner = false)
        {
            _currentCoord = null;

            foreach (var cellCondition in CoordDescriptor.ExcludeCellConditions)
            {
                if (resetOwner || cellCondition.Owner != this)
                {
                    cellCondition.Owner = this;
                }
                else
                {
                    cellCondition.Reset();
                }
            }
        }
    }
}