﻿using System.Collections;
using System.Collections.Generic;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Excel.Reader
{
    /// <summary>
    /// this is the standard reader to use IRecordIterators with the following canonical column names
    /// CODE FOR EACH DIM
    /// DATA_VALUE
    /// in that given order
    /// </summary>
    public class SWCanonicalReader<T> : SWRecordIteratorReader<T> where T:class
    {
        private bool _Exhausted;
        private readonly string _Name;

        public SWCanonicalReader(IRecordIterator<T> recordIterator, Dataflow ds,string name = null, bool shouldDisposeIterator = true)
            : base(recordIterator, ds, shouldDisposeIterator)
        {
            _Exhausted = false;
            _Name = name;
        }

        protected override bool FetchNext()
        {
            if (_Exhausted)
                return false;

            _Exhausted = !RecordIterator.Read();

            return !_Exhausted;
        }

        public override int CurrentRecordNo
        {
            get { return RecordIterator.CurrentRecordNo; }
        }

        public override string Name
        {
            get { return _Name; }
        }

        public override void Reset()
        {
            _Exhausted = false;
            this.RecordIterator.Reset();
        }

        public IEnumerable<T> AsIEnumerable()
        {
            return new DummyEnumerable<T>(this);
        }

        private class DummyEnumerable<R> : IEnumerable<R>
        {
            private readonly IEnumerator<R> _enumerator;

            public DummyEnumerable(IEnumerator<R> e)
            {
                _enumerator = e;
            }
            public IEnumerator<R> GetEnumerator()
            {
                _enumerator.Reset();

                return _enumerator;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}